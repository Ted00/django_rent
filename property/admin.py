from django.contrib import admin

# Register your models here.
from property.models import Owner, Home

admin.site.register(Owner)
admin.site.register(Home)
