from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django_filters.views import BaseFilterView

from property.filters import HomeFilter
from property.forms import HomeForm
from property.models import Home, Owner


class HomeListView(ListView, BaseFilterView):
    model = Home
    template_name = 'home.html'
    filterset_class = HomeFilter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['home'] = Home.objects.filter()
        context['filter'] = HomeFilter
        return context

    @staticmethod
    def product_list(request):
        f = HomeFilter(request.GET, queryset=Home.objects.all())
        return render(request, 'home.html', {'filter': f})


class OwnerListView(ListView):
    model = Owner
    template_name = 'owners.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['home'] = Home.objects.filter()
        context['owner'] = Owner.objects.filter()
        return context


class HomeDetailView(DetailView):
    model = Home
    template_name = 'home_details.html'


class HomeUpdateView(UpdateView):
    model = Home
    template_name = 'home_update.html'
    fields = ['area', 'address', 'city', 'state', 'rent', 'owner']
    context_object_name = 'updatehome'


class HomeCreateView(CreateView):
    model = Home
    template_name = 'home_update.html'
    form_class = HomeForm
    context_object_name = 'createhome'


class HomeDeleteView(DeleteView):
    template_name = 'home_delete.html'
    model = Home
    context_object_name = 'deletepost'
    success_url = reverse_lazy('home')


def product_list(request):
    f = HomeFilter(request.GET, queryset=Home.objects.all())
    return render(request, 'home.html', {'filter': f})
