import django_filters
from django.forms.widgets import TextInput

from property.models import Home


class RangeWidget(django_filters.widgets.RangeWidget):

    def __init__(self, from_attrs=None, to_attrs=None, attrs=None):
        super(RangeWidget, self).__init__(attrs)
        if from_attrs:
            self.widgets[0].attrs.update(from_attrs)
        if to_attrs:
            self.widgets[1].attrs.update(to_attrs)


class HomeFilter(django_filters.FilterSet):
    city = django_filters.CharFilter(lookup_expr='iexact', widget=TextInput(attrs={'placeholder': 'City'}))
    area = django_filters.NumberFilter(widget=TextInput(attrs={'placeholder': 'Area'}))
    state = django_filters.CharFilter(lookup_expr='iexact', widget=TextInput(attrs={'placeholder': 'State'}))
    address = django_filters.CharFilter(lookup_expr='icontains', widget=TextInput(attrs={'placeholder': 'Address'}))
    rent = django_filters.RangeFilter(
        label='Price',
        widget=RangeWidget(
            from_attrs={'placeholder': 'From', 'class': 'first'},
            to_attrs={'placeholder': 'To', 'class': 'second'},
        )
    )

    class Meta:
        model = Home
        fields = {
            'area',
            'address',
            'city',
            'state',
            'rent',
            'owner',
        }
        # rent = django_filters.NumberFilter(attrs={'class': 'main'})
